ruby-ole (1.2.12.2-2) unstable; urgency=medium

  * Team upload

  [ Gunnar Wolf ]
  * Removing myself from uploaders, acknowledging reality

  [ HIGUCHI Daisuke (VDR dai) ]
  * d/control: Bump Standards-Version to 4.6.1
  * eliminate lintian warning: update-debian-copyright

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Mon, 08 Aug 2022 11:03:02 +0900

ruby-ole (1.2.12.2-1) unstable; urgency=medium

  * Team upload

  [ Cédric Boutillier ]
  * Update team name

  [ Sergio de Almeida Cipriano Junior ]
  * New upstream version 1.2.12.2
  * d/control:
    - Bump debhelper-compat to 13
    - Bump Standards-Version to 4.5.1
  * Add new README in d/ruby-ole.docs

 -- Sergio de Almeida Cipriano Junior <sergiosacj@hotmail.com.br>  Wed, 20 Jan 2021 08:53:46 -0300

ruby-ole (1.2.11.8-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Daniel Leidert ]
  * d/control: Add Testsuite and Rules-Requires-Root field.
    (Uploaders): Remove Ryan Niebur (closes: #856368). Thanks for your work.
    (Standards-Version): Bump to 4.5.0.
    (Depends): Use ${ruby:Depends} and remove interpreter.
  * d/copyright (Copyright): Add team.
  * d/ruby-test-files.yaml: Replace file by d/ruby-tests.rake.
  * d/ruby-tests.rake: Add new rake test file (closes: #952043).
  * d/watch: Use github release page as source.
  * d/patches/*: Remove unused patches and empty series file.
  * d/upstream/metadata: Add YAML frontmatter, Archive and Changelog fields.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 26 Mar 2020 16:24:17 +0100

ruby-ole (1.2.11.8-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Christian Hofstaedtler ]
  * Upload to unstable.

 -- Christian Hofstaedtler <zeha@debian.org>  Fri, 04 Mar 2016 19:13:06 +0100

ruby-ole (1.2.11.8-1~exp1) experimental; urgency=medium

  * Team upload.
  * Target experimental and build with ruby 2.2.
  * Add ruby-test-unit to Build-Depends for ruby2.2
  * Update Vcs-Browser to cgit URL and HTTPS

 -- Sebastien Badia <seb@sebian.fr>  Sun, 12 Apr 2015 21:32:52 +0200

ruby-ole (1.2.11.7-3) unstable; urgency=medium

  * Team upload.
  * Rebuild to add rubygems-integration support for all Ruby versions.

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 20 Oct 2014 12:17:17 -0200

ruby-ole (1.2.11.7-2) unstable; urgency=low

  * Team upload.
  * d/control:
    - removed transitional packages
    - bumped standards version to 3.9.5 (no changes needed)
    - removed version dependency of ruby1.8, changed to ruby
    - wrap-sort Uploaders

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Sat, 30 Nov 2013 20:39:39 +0100

ruby-ole (1.2.11.7-1) unstable; urgency=low

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0
    official URL for Format field

  [ Youhei SASAKI ]
  * Imported Upstream version 1.2.11.7
  * Bump Standard Version: 3.9.4
  * Update debian/rules:
    - simplify override_dh_auto_install
    + add ruby-ole.examples
  * Drop obsolete patch

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 06 Aug 2013 00:14:52 +0900

ruby-ole (1.2.11.3-1) unstable; urgency=low

  * Imported Upstream version 1.2.11.3
  * Bump Standard Version: 3.9.3
  * Unapply patches after build
  * Drop patch: dont_use_deprecated_new0
    - merge upstream
  * Update patch: Add DEP-3 header

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 26 Mar 2012 00:29:50 +0900

ruby-ole (1.2.11.2-1) unstable; urgency=low

  * New upstream release: 1.2.11.2
  * Add me to Uploaders

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 22 Jan 2012 05:03:29 +0900

ruby-ole (1.2.11.1-6) unstable; urgency=low

  * Transitional packages should be oldlibs/extra as requested by
    lintian
  * Fix FTBFS due to a call to a no-longer-provided interface
    (DateTime#new!) under Ruby 1.9.1 (Closes: #652752)

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 16 Jan 2012 00:33:27 -0600

ruby-ole (1.2.11.1-5) unstable; urgency=low

  [Antonio Terceiro]
  * Team upload (not a NMU).
  * Replace Conflicts: with Breaks: in debian/control

  [Youhei SASAKI]
  * Fix FTBFS: (Closes: #634449)
    - debian/patches/move_propids_location: fix filepath
    - debian/rules: running "dh_install data/*" before dh_ruby --install

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 27 Jul 2011 16:17:40 +0900

ruby-ole (1.2.11.1-4) unstable; urgency=low

  * Repackaged with the gem2deb packaging infrastructure

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 16 May 2011 09:50:57 -0500

libole-ruby (1.2.11.1-3) unstable; urgency=low

  * Uff.. It's quite embarassing to admit my upload targetted to
    unstable was... sent to experimental :-P

 -- Gunnar Wolf <gwolf@debian.org>  Wed, 09 Feb 2011 13:41:13 -0600

libole-ruby (1.2.11.1-2) experimental; urgency=low

  * Squeeze freeze is over - Reupload, targetting at unstable instead of
    experimental

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 08 Feb 2011 20:58:03 -0600

libole-ruby (1.2.11.1-1) experimental; urgency=low

  * New upstream release
  * Targetting experimental in order not to interfere with Squeeze's
    freeze
  * Standards-version 3.8.4→3.9.1.0 (no changes needed)

 -- Gunnar Wolf <gwolf@debian.org>  Wed, 24 Nov 2010 09:37:51 -0600

libole-ruby (1.2.9-5) UNRELEASED; urgency=low

  * debian/watch: updated for the Google Code webpage changes.

 -- Paul van Tilburg <paulvt@debian.org>  Sun, 16 May 2010 12:03:43 +0200

libole-ruby (1.2.9-4) unstable; urgency=low

  * Port the package to Ruby 1.9.1 (Closes: #569867)
  * Standards-version 3.8.3→3.8.4 (no changes needed)
  * Switched to 3.0 (quilt) source format; dropped build-dependency on
    quilt
  * Added DEP3 headers to move_propids_location patch

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 15 Feb 2010 17:08:40 -0600

libole-ruby (1.2.9-3) unstable; urgency=low

  * rubygems1.9 was renamed to rubygems1.9.1, update dependencies
    (Closes: #550365)
  * update my email address

 -- Ryan Niebur <ryan@debian.org>  Mon, 19 Oct 2009 17:20:06 -0700

libole-ruby (1.2.9-2) unstable; urgency=low

  * Debian Policy 3.8.3
  * add README.source
  * Add myself to Uploaders
  * add build dep on ruby1.9 (Closes: #543040)

 -- Ryan Niebur <ryanryan52@gmail.com>  Sun, 23 Aug 2009 11:25:13 -0700

libole-ruby (1.2.9-1) unstable; urgency=low

  * New upstream release
  * Create a binary package for Ruby 1.9 as well
  * Standards-version → 3.8.2 (no changes needed)
  * Oletool has been moved to become an example, not a regular
    executable

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 21 Jul 2009 18:55:43 +0200

libole-ruby (1.2.8.2-1) unstable; urgency=low

  * Initial upload (Closes: #525026)

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 21 Apr 2009 13:24:41 -0500
